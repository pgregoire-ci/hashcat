# hashcat

Binary builds of https://github.com/hashcat/hashcat/


## Run-time dependencies

```
sudo apt-get install libminizip1 libxxhash0 pocl-opencl-icd zlib1g
sudo ldconfig /usr/local/lib/
```


## Builds

https://gitlab.com/pgregoire-ci/hashcat/-/jobs/artifacts/main/raw/hashcat.tar.gz?job=build
